package com.example.pirang.asyntask;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading");
        dialog.setCancelable(false);

        findViewById(R.id.btnClick).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MyAsynTask().execute("https://jsonplaceholder.typicode.com/posts");
            }
        });
    }


    class MyAsynTask extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Work on Main Thread
            dialog.show();

        }

        @Override
        protected String doInBackground(String... strings) {

            String str = "";

            try {
                URL url = new URL(strings[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");

                InputStreamReader inputStreamReader = new InputStreamReader(connection.getInputStream());

                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                int c;
                while ((c = bufferedReader.read()) != -1) {
                    str += String.valueOf((char) c);
                }
                return str;

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            Log.e("ooooo", String.valueOf(values[0]));
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.dismiss();

            try {
                JSONArray jsonArray = new JSONArray(result);
                Log.e("ooooo", jsonArray.length() + "");
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }


//    class MyAsynTask extends AsyncTask<String, Integer, String> {
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            // Work on Main Thread
//            dialog.show();
//
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//
//            int num = 0;
//            for (int i = 0; i <= 10; i++) {
//                try {
//                    Thread.sleep(1000);
//                    publishProgress(i);
//                    num = i;
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//            return strings[0] + num;
//        }
//
//        @Override
//        protected void onProgressUpdate(Integer... values) {
//            super.onProgressUpdate(values);
//            Log.e("ooooo", String.valueOf(values[0]));
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            super.onPostExecute(result);
//            dialog.dismiss();
//            Toast.makeText(MainActivity.this, result, Toast.LENGTH_SHORT).show();
//        }
//    }

}
